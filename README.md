# Computational Geometry Examples
This are the small codes which shows working of Computational Geometry Examples in R.

**Convex Hull:**
Here I used the Sphere for the creation of points and than created Convex Hull and using SGL package I created 3D Visualization.

**Delaunay Triangulation:**
I created Cube and then used in build function to create Delaunay Triangulation. I created 3D Visualization using RGL Package.

**Prerequisites**

*Libraries*
* geometry -- Mesh Generation and Surface Tesselation 
  + Installation:```package.install("geometry")```
* rgl -- 3D Visualization using OpenGL
  + Installation:```package.install("rgl")```
* testthat -- Unit testing in R
  + Installation:```package.install("testthat")```
* matlab -- MATLAB imulation package
  + Installation:```package.install("matlab")```
  
**Errors that might appear:**
1. LoadLibrary failed with error 1114
      + Your Switchable Graphics are not working on full performance. Change it from *Advanced Power Setting.*
      + This [Youtube Video](https://www.youtube.com/watch?v=fJXPXgEXvcU) will solve the issue
